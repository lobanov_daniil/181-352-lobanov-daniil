// lab3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>

#include "pch.h"
#include <iostream>


//Использование пространственного имени std.

using namespace std;

int main()
{

	//Объявление переменых
	int i, k, sum, a;
	int MatrixA[3][3] = { {1,2,3},
	{4,5,6},
	{7,8,9} };

	int MatrixB[3][3] = { {1,2,3},
	{4,5,6},
	{7,8,9} };

	int MatrixC[3][3] = { {0} };
	int Vector[3] = { 5,10,2 };
	sum = 0;

	//Вывод матрицы 1
	cout << "Умножение матрицы" << endl;
	for (i = 0; i < 3; i++) {

		for (k = 0; k < 3; k++) {

			cout << MatrixA[i][k] << ' ';
		}

		cout << endl;
	}

	//Вывод матрицы 2 
	cout << "На матрицу" << endl;
	for (i = 0; i < 3; i++) {

		for (k = 0; k < 3; k++) {

			cout << MatrixA[i][k] << ' ';
		}

		cout << endl;
	}
	cout << "Равно" << endl;

	//Умножение матриц
	for (i = 0; i < 3; i++) {

		for (k = 0; k < 3; k++) {

			MatrixC[i][k] = MatrixA[i][k] * MatrixB[k][i];
			cout << MatrixC[i][k] << ' ';
		}

		cout << endl;
	}
	cout << endl;

	//Поэлементное произведение матриц
	cout << "Поэлементное произведение матриц равно" << endl;
	for (i = 0; i < 3; i++) {

		for (k = 0; k < 3; k++) {

			MatrixC[i][k] = MatrixA[i][k] * MatrixB[i][k];
			cout << MatrixC[i][k] << ' ';
		}

		cout << endl;
	}
	cout << endl;

	//Вывод вектора
	cout << "Произведение верхней матрицы на вектор" << endl;
	for (i = 0; i < 3; i++) {
		cout << Vector[i] << endl;
	}
	cout << "Равно" << endl;

	//Умножение матрицы на вектор
	for (i = 0; i < 3; i++) {

		for (k = 0; k < 3; k++) {

			a = Vector[i] * MatrixC[i][k];
			sum = sum + a;
		}

		Vector[i] = sum;
		sum = 0;
		cout << Vector[i] << endl;
	}

	return 0;
}